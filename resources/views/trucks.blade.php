<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Trucks') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <a href="{{url('import-trucks')}}" class="btn btn-sm btn-outline-dark">Import trucks</a>
            <table class="table table-dark table-hover my-6">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Truck number</th>
                    <th scope="col">Last used</th>
                    <th scope="col">Location</th>
                </tr>
                </thead>
                <tbody>
                @php $i=1 @endphp
                @foreach($trucks as $key => $data)
                    @if(date("Y-m-d",$data->lastUsed) == date("Y-m-d"))
                        <tr>
                            <th scope="row">{{$i++}}</th>
                            <td>{{$data->name}}</td>
                            <td>{{date("Y-m-d H:i:s",$data->lastUsed)}}</td>
                            <td><a href="{{'https://maps.google.com/?q='.$data->lat.','.$data->lng}}" target="_blank" class="btn btn-sm btn-light">Check map</a></td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>
