<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('All files') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-3xl mx-auto sm:px-6 lg:px-8">
            <table class="table table-dark table-hover my-6 dashboard">
                <thead>
                <tr>
                    <th scope="col">File/Driver</th>
                    <th scope="col">Date</th>
                    <th scope="col">Container</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($files))
                    @foreach($files as $key => $data)
                        <tr>
                            <td><a href="/download/{{$data->getBasename()}}">{{$data->getBasename()}}</a></td>
                            <td>In progress</td>
                            <td>In progress</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>
