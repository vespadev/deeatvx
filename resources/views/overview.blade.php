<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Overview') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-9xl mx-auto sm:px-6 lg:px-8">
            <form method="post" action="{{ route('overview') }}" id="form_data">
                @csrf
                <select name="driver_id" id="test123">
                    @foreach( $drivers as $key => $driver)
                        <option value="{{$driver->id}}">{{$driver->name}}</option>
                    @endforeach
                </select>
                <div class="flex items-center justify-end mt-4">

                    <x-jet-button class="ml-4 mx-auto">
                        {{ __('Get driver') }}
                    </x-jet-button>
                </div>
            </form>
            <table class="table table-dark table-hover my-6 dashboard">
                <thead>
                <tr>
                    <th scope="col">Driver</th>
                    <th scope="col">Truck</th>
                    <th scope="col">Driving time</th>
                    <th scope="col">Stop Time</th>
                    <th scope="col">Miles</th>
                    <th scope="col">Night Out</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($jobs))
                    @foreach($jobs as $key => $data)
                        <tr class="{{$data['night_out'] }}">
                            <th>{{$data['driver_name']}}</th>
                            <th>{{$data['truck_name']}}</th>
                            <td>{{$data['drives_duration']}}</td>
                            <td>{{$data['stops_duration']}}</td>
                            <td>{{$data['route_length']}}</td>
                            <td>{{$data['weekday']}} // {{$data['created_at']}}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>
