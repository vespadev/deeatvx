<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12 dashboard_x">
        <div class="max-w-9xl mx-auto sm:px-6 lg:px-6">
            <div class="container">
                <div class="row">
                    @if(isset($jobs))
                        @foreach($jobs as $key => $data)
                            <div class="col-sm-4" style="margin-bottom:25px">
                                <div class="card {{$data['truck']['status_cargo']}}">
                                    <div class="card-header">
                                        <div class="card-head-left">
                                            <h2>{{$data['indice']}}</h3>
                                                <h4>{{$data['driver']}}</h4>
                                                <p>{{$data['truck']['address']}}</p>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="infos">
                                            <div class="info-note">
                                                <h4>{{$data['truck']['name']}}</h4>
                                                <p>Truck</p>
                                            </div>
                                            <div class="info-note">
                                                <h4>{{$data['truck']['drive_time']}}</h4>
                                                <p>Drive time</p>
                                            </div>
                                            <div class="info-note">
                                                <h4>{{$data['truck']['delivery_time']}}</h4>
                                                <p>Delivery time</p>
                                            </div>
                                        </div>
                                        <div class="infos">
                                            <div class="info-note">
                                                <h4>{{$data['truck']['status']}}</h4>
                                                <p>Status truck</p>
                                            </div>
                                            <div class="info-note">
                                                <h4>{{$data['post_code']}}</h4>
                                                <p>Destination</p>
                                            </div>
                                            <div class="info-note">
                                                <h4>{{$data['eta_time']}}</h4>
                                                <p>ETA</p>
                                            </div>
                                        </div>
                                        <div class="infos">
                                            <div class="info-note">
                                                <h4>{{$data['truck']['status_cargo']}}</h4>
                                                <p>Status cargo</p>
                                            </div>
                                            <div class="info-note">
                                                <h4>
                                                    @if(isset($data['duration']['eta_miles']))
                                                        {{$data['duration']["eta_miles"]}}
                                                    @else
                                                        Refreshing...
                                                    @endif
                                                </h4>
                                                <p>Miles distance</p>
                                            </div>
                                            <div class="info-note">
                                                <h4>
                                                    @if(isset($data['duration']['eta_time']))
                                                        {{$data['duration']["eta_time"]}}
                                                    @else
                                                        Refreshing...
                                                    @endif
                                                </h4>
                                                <p>Time distance</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="btngroup">
                                            <a href="https://www.google.com/maps?saddr={{$data['truck']['lat']}},{{$data['truck']['lng']}}&daddr={{$data['post_code']}}"
                                               target="_blank" class="btn">Check map</a>
                                            @if(Auth::user()->current_team_id > 2)
                                                <a href="{{url('/form-edit-status/'.$data['truck']['truck_id'])}}"
                                                   class="btn ">Change status</a>
                                                <a href="{{url('/form-update-address/'.$data['truck']['truck_id'])}}"
                                                   class="btn ">Update address</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
