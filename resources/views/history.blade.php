<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('History') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <a href="{{url('form-add-history')}}" class="btn btn-sm btn-outline-dark ">Add history</a>
            <table class="table table-dark table-hover my-6">
                <thead>
                <tr>
                    <th scope="col">Sofer</th>
                    <th scope="col">Data</th>
                    <th scope="col">Container/Frigorifc</th>
                    <th scope="col">Adresa</th>
                    <th scope="col">VBS</th>
                    <th scope="col">Miles</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($jobs))
                    @foreach($jobs as $key => $data)
                        <tr>
                            <td scope="row"></td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>
