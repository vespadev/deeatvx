<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Update job') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-2xl mx-auto py-10 sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('update-job') }}">
                @csrf
                <div>
                    <x-jet-label for="driver" value="{{ __('Driver') .' | Current driver: '. $current_data['driver']}}" />
                    <select name="driver_id" id="driver" class="form-select rounded-md shadow-sm mt-1 block w-full">
                        <option value="0">No Change</option>
                        @foreach($drivers as $driver)
                            <option value="{{$driver->id}}">{{$driver->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="mt-4">
                    <x-jet-label for="post_code" value="{{ __('Postal code')}}" />
                    <x-jet-input id="post_code" class="block mt-1 w-full" type="text" name="post_code" value="{{$current_data['post_code']}}" required />
                </div>

                <div class="mt-4">
                    <x-jet-label for="truck" value="{{ __('Truck').' | Current truck: '. $current_data['truck'] }}" />
                    <select name="truck" id="truck" class="form-select rounded-md shadow-sm mt-1 block w-full">
                        <option value="0">No Change</option>
                        @foreach($trucks as $truck)
                            <option value="{{$truck->id}}">{{$truck->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="mt-4">
                    <x-jet-label for="indice" value="{{ __('Indice').' | Current indice: '. $current_data['indice'] }}" />
                    <select name="indice" id="indice" class="form-select rounded-md shadow-sm mt-1 block w-full">
                        <option value="0">No Change</option>
                        @foreach($indici as $indice)
                            <option value="{{$indice->id}}">{{$indice->indice}}</option>
                        @endforeach
                    </select>
                </div>
                    <input type="hidden" name="id" value="{{$current_data['id']}}">
                <div class="flex items-center justify-end mt-4">

                    <x-jet-button class="ml-4 mx-auto">
                        {{ __('Update job') }}
                    </x-jet-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
