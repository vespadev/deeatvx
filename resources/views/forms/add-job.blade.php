<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add new job') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-2xl mx-auto py-10 sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('save-job') }}">
                @csrf
                <div>
                    <x-jet-label for="driver" value="{{ __('Name') }}" />
                    <select name="driver_id" id="driver" class="form-select rounded-md shadow-sm mt-1 block w-full">
                        @foreach($drivers as $driver)
                            <option value="{{$driver->id}}">{{$driver->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="mt-4">
                    <x-jet-label for="post_code" value="{{ __('Postal code') }}" />
                    <x-jet-input id="post_code" class="block mt-1 w-full" type="text" name="post_code" required />
                </div>

                <div class="mt-4">
                    <x-jet-label for="truck" value="{{ __('Truck') }}" />
                    <select name="truck" id="truck" class="form-select rounded-md shadow-sm mt-1 block w-full">
                        @foreach($trucks as $truck)
                            <option value="{{$truck->id}}">{{$truck->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="mt-4">
                    <x-jet-label for="indice" value="{{ __('Indice') }}" />
                    <select name="indice" id="indice" class="form-select rounded-md shadow-sm mt-1 block w-full">
                        @foreach($indici as $indice)
                            <option value="{{$indice->id}}">{{$indice->indice}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="flex items-center justify-end mt-4">

                    <x-jet-button class="ml-4 mx-auto">
                        {{ __('Add job') }}
                    </x-jet-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
