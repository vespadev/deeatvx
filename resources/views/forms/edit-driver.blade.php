<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Update driver') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-2xl mx-auto py-10 sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('update-driver') }}">
                @csrf
                <div>
                    <x-jet-label for="name" value="{{ __('Name') }}" />
                    <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" value="{{$driver->name}}" required autofocus />
                </div>

                <div class="mt-4">
                    <x-jet-label for="phone" value="{{ __('Phone') }}" />
                    <x-jet-input id="phone" class="block mt-1 w-full" type="text" name="phone" value="{{$driver->phone}}" required autocomplete="current-password" />
                </div>
                    <input type="hidden" name="id" value="{{$driver->id}}">
                <div class="flex items-center justify-end mt-4">

                    <x-jet-button class="ml-4 mx-auto">
                        {{ __('Edit driver') }}
                    </x-jet-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
