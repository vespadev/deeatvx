<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Update job') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-2xl mx-auto py-10 sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('update-delivery-address') }}">
                @csrf
                <div>
                    <input type="hidden" name="truck_id" value="{{$id}}">
                </div>
                <div class="mt-4">
                    <x-jet-label for="truck_name" value="{{ __('Truck name')}}" />
                    <x-jet-input id="truck_name" class="block mt-1 w-full" type="text" name="truck_name" value="{{$name}}" disabled />
                </div>
                <div class="mt-4">
                    <x-jet-label for="post_code" value="{{ __('Postal code')}}" />
                    <x-jet-input id="post_code" class="block mt-1 w-full" type="text" name="post_code" value="{{$post_code}}" required />
                </div>
                <div class="mt-4">
                    <x-jet-label for="post_code" value="{{ __('Delivery Time')}}" />
                    <x-jet-input id="post_code" class="block mt-1 w-full" type="text" name="delivery_time" value="{{$delivery_time}}" required />
                </div>
                <div class="flex items-center justify-end mt-4">

                    <x-jet-button class="ml-4 mx-auto">
                        {{ __('Update address') }}
                    </x-jet-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
