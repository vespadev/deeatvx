<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Change Status') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-2xl mx-auto py-10 sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('update-status') }}">
                @csrf
                <input type="hidden" name="truck" value="{{$truck->id}}">
                <input type="hidden" name="post_code" value="{{$post_code}}">
                <div class="mt-4">
                    <x-jet-label for="cargo" value="{{ __('Cargo Status')}}" />
                    <select name="cargo_status" id="cargo" class="form-select rounded-md shadow-sm mt-1 block w-full">
                        <option value="Loaded">Loaded</option>
                        <option value="Tipping">Tipping</option>
                        <option value="Empty">Empty</option>
                    </select>
                </div>
                <div class="flex items-center justify-end mt-4">

                    <x-jet-button class="ml-4 mx-auto">
                        {{ __('Update job') }}
                    </x-jet-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
