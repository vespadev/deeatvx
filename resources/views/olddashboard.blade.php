<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12 dashboard_x">
        <div class="max-w-9xl mx-auto sm:px-6 lg:px-8">
            <table class="table table-dark table-hover my-6 dashboard">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Status</th>
                    <th scope="col">Driving time</th>
                    <th scope="col">ORA LIVRARE</th>
                    <th scope="col">Current Address</th>
                    <th scope="col">Destination</th>
                    <th scope="col">Distance</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($jobs))
                    @foreach($jobs as $key => $data)
                    
                        <tr truck="{{$data['truck']['name']}}">
                            <th scope="row">{{$data['indice']}}<br>{{$data['driver']}}</th>
                            <td scope="status">{{$data['truck']['status']}}</td>
                            <td scope="driver_time">{{$data['truck']['drive_time']}}</td>
                            <td scope="cargo_status">{{$data['truck']['status_cargo']}}</td>
                            <td scope="address" style="max-width: 150px">{{$data['truck']['address']}}</td>
                            <td scope="destination">{{$data['post_code']}}</td>
                            <td scope="distance">
                            @if(isset($data['duration']['eta_time']))
                            {{$data['duration']['eta_time']}} <br> {{$data['duration']["eta_miles"]}}
                                @else
                                Refreshing...
                            @endif
                            </td>
                            <td scope="maps">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="https://www.google.com/maps?saddr={{$data['truck']['lat']}},{{$data['truck']['lng']}}&daddr={{$data['post_code']}}"
                                       target="_blank" class="btn btn-sm btn-secondary btn-light">Check map</a>
                                    @if(Auth::user()->current_team_id > 2)
                                        <a href="{{url('/form-edit-status/'.$data['truck']['truck_id'])}}"
                                           class="btn btn-sm btn-secondary">Change status</a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>
