<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Jobs') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <a href="{{url('form-add-job')}}" class="btn btn-sm btn-outline-dark ">Add jobs</a>
            <table class="table table-dark table-hover my-6">
                <thead>
                <tr>
                    <th scope="col">Indice + Driver</th>
                    <th scope="col">Truck</th>
                    <th scope="col">Destination</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($jobs))
                    @foreach($jobs as $key => $data)
                        <tr>
                            <th scope="row">{{$data['indice'] . ' - ' . $data['driver']}}</th>
                            <td>{{$data['truck']}}</td>
                            <td>{{$data['post_code']}}</td>
                            <td>
                                <div class="btn-group btn-group-sm" role="group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-light dropdown-toggle"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                        Action
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                        <li><a class="dropdown-item" href="{{url('/edit-job/'.$data['id'])}}">Edit</a></li>
{{--                                        <li><a class="dropdown-item" href="{{url('/delete-job/'.$data['id'])}}">Delete</a></li>--}}
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>
