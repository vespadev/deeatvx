<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trucks extends Model
{
    use HasFactory;

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'serial_truck',
        'status',
        'status_cargo',
        'drive_time',
        'current_address',
        'lat',
        'lng',
		'delivery_time'
    ];
}
