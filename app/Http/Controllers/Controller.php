<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Auth;
use phpseclib3\Math\Common\FiniteField\Integer;

class Controller extends BaseController
{

	private function calculator_eta($time) {

		if($time['eta_time'] != "Reloading") {
			$now = date("H:i");
			$hours = 0;
			$arr = explode(':',$now);
			if (strlen($time['eta_time'])>6) {
				$mins = substr($time['eta_time'], -6);
				$hours = substr($time['eta_time'],0,4);
				$hours = explode(' ',$hours)[0];
			} else {
				$mins = substr($time['eta_time'], -6);
			}
			$mins = (int)$arr[1]+(int)$mins;
			if ($mins >60) {
				$mins -= 60;
				$hours +=1;
			}
			$hours = (int)$arr[0]+(int)$hours;
			if ($hours >= 24) {
				$hours -= 24;
			}
			$text = $hours.":".$mins;
			return $text;
		}
	}

    public function index()
    {
        $jobs = DB::table('job_status')->orderBy('indice_id', 'asc')->get();
        foreach ($jobs as $job) {
            $indice = JobsController::getIndice($job->indice_id);
            $truck = $this->getCoords($job->truck_id);
            $eta = GpsController::getETAbyID($job->truck_id);
            $driver = $this->getDriver($job->driver_id);
            $array[] = $this->build_array($truck,$indice, $job->post_code, $eta, $driver);
        }
        if(Auth::user()->current_team_id == 999){
        return view('dashboard', [
            'jobs' => $array
        ]);
        } else {
            return redirect("/send-job");
        }
    }
    private function build_array($truck,$indice, $post_code, $eta, $driver) {
		$eta_time = $this->calculator_eta($eta);
        return [
            'truck' => $truck,
            'indice' => $indice,
            'post_code' => $post_code,
            'duration' => $eta,
            'driver' =>$driver,
			'eta_time'=>$eta_time
        ];
    }
    private function getCoords($id)
    {
        $id = DB::table('trucks')->where("id","=",$id)->get();
        $array = [
            'truck_id' => $id[0]->id,
            'name' => $id[0]->name,
            'status' => $id[0]->status,
            'status_cargo' => $id[0]->status_cargo,
            'drive_time' => $id[0]->drive_time,
            'address' => $id[0]->current_address,
            'lat' => $id[0]->lat,
            'lng' => $id[0]->lng,
			'delivery_time' => $id[0]->delivery_time
        ];
        return $array;
    }
	public function updateAddressForm($id)
	{
		$truck= DB::table('trucks')->find($id);
		$post_code =  DB::table('job_status')->where('truck_id','=',$id)->select('post_code')->get();
		return view('forms.update-address', [
			'id' => $id,
			'name' => $truck->name,
			'delivery_time' => $truck->delivery_time,
			'post_code' => $post_code[0]->post_code
		]);
	}

	public function updateAddress(Request $request)
	{
		DB::table('trucks')
			->where('id', '=', $request->input('truck_id'))
			->update([
				'status_cargo' => 'Loaded',
				'delivery_time' => $request->input('delivery_time'),
			]);
		DB::table('job_status')
			->where('truck_id', '=', $request->input('truck_id'))
			->update([
				'post_code' => $request->input('post_code'),
			]);
		return redirect('/dashboard');
	}

	public function changeStatus($id)
	{
		DB::table('trucks')
			->where('id', '=', $id)
			->update([
				'status_cargo' => 'Empty'
			]);

		DB::table('job_status')
			->where('truck_id', '=', $id)
			->update([
				'post_code' => "IP11 3QX"
			]);
		return redirect('/dashboard');
	}

    public function addHistory()
    {
        return view('forms.add-history');
    }

    public function saveHistory(Request $request)
    {
        $file = $request->file('file')->getRealPath();
        $datas = array_map('str_getcsv', file($file));
        foreach (array_splice($datas, 1) as $data) {
            $driver = substr($data[4], -7);
            $postcode = $data[10];
            $id = DB::table('trucks')->where('name', '=', $driver)->first();
            if (isset($id)) {
                DB::table('job_status')
                    ->where('truck_id', '=', $id->id)
                    ->update([
                        'post_code' => $postcode,
                        'status_cargo' => "Loaded"
                    ]);
            }
        }
        return redirect('/history');
    }

    public function overview(Request $request) {
        if (!$request->input('driver_id')) $id = 1;
        else $id = $request->input('driver_id');
        $jobs = DB::table('overview')
            ->where('id_driver','=', $id)
            ->orderBy('created_at')->get();
        $drivers = DB::table('drivers')->get();
        $array = [];
        foreach ($jobs as $job) {
            $info = $this->getCoords($job->id_truck);
            $driver = $this->getDriver($job->id_driver);
            $truck['truck_name'] = $info['name'];
            $truck['driver_name'] = $driver;
            $truck['drives_duration'] = $job->drives_duration;
            $truck['stops_duration'] = $job->stops_duration;
            $truck['route_length'] = $job->route_length;
            $truck['created_at'] = $job->created_at;
            $truck['weekday'] = date('l', strtotime($job->created_at));
            $truck['night_out'] = "confirmed_".$job->night_out;
            if ($truck['route_length'] > 0)
                $array[] = $truck;
        }
        return view('overview', [
            'jobs' => $array,
            'drivers' => $drivers
        ]);
    }

    private function getDriver($id) {
        $driver = DB::table('drivers')->find($id);
        return $driver->name;
    }
}
