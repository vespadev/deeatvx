<?php

namespace App\Http\Controllers;

use App\Models\JobStatus;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $jobs= DB::table('job_status')->orderBy('indice_id', 'asc')->get();

        foreach ($jobs as $job) {
            $array[] = [
                'id' => $job->id,
                'driver' => $this->getDriverName($job->driver_id),
                'truck' => $this->getTruckName($job->truck_id),
                'indice' => $this->getIndice($job->indice_id),
                'post_code' => $job->post_code,
            ];
        }
        return view('jobs', [
            'jobs' => $array
        ]);
    }

    protected static function getIndice($id):string {
        $id = DB::table('indici')->find($id);
        return $id->indice;
    }

    protected static function getDriverName($id):string {
        $id = DB::table('drivers')->find($id);
        return $id->name;
    }

    protected static function getTruckName($id):string {
        $idx = DB::table('trucks')->find($id);
        return $idx->name;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function createJob(Request $request)
    {
        DB::table('job_status')->insertOrIgnore([
            'driver_id' => $request->input('driver_id'),
            'post_code' => $request->input('post_code'),
            'truck_id' => $request->input('truck'),
            'indice_id' => $request->input('indice')
        ]);
        return redirect('/jobs')->with('message', "Was created success!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JobStatus  $jobStatus
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job= DB::table('job_status')->find($id);
        $current_data['id'] = $id;
        $current_data['driver'] = $this->getDriverName($job->driver_id);
        $current_data['truck'] = $this->getTruckName($job->truck_id);
        $current_data['indice'] = $this->getIndice($job->indice_id);
        $current_data['post_code'] = $job->post_code;
        $drivers = DB::table('drivers')->get();
        $list = DB::table('status_list')->get();
        $trucks = DB::table('trucks')->get();
        $indici = DB::table('indici')->get();
        return view('forms.edit-job', [
            'drivers' => $drivers,
            'status_list' => $list,
            'trucks' => $trucks,
            'indici' => $indici,
            'current_data' => $current_data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JobStatus  $jobStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $job = JobStatus::find($request->input('id'));
        if ($request->input('driver_id') != 0)
            $job->fill(['driver_id' =>$request->input('driver_id')]);
        $job->fill(['post_code' =>$request->input('post_code')]);
        if ($request->input('truck') != 0)
            $job->fill(['truck_id' =>$request->input('truck')]);
        if ($request->input('indice') != 0)
            $job->fill(['indice_id' =>$request->input('indice')]);
        $job->save();
        return redirect('/jobs')->with('message', "Was updated success!");
    }

    public function createJobPdf(Request $request) {
        $file = $request->file('file');
        $filename = time() . '.' . $request->file('file')->extension();
        $filePath = public_path() . '/files/';
        $file->move($filePath, $filename);
        return redirect('/send-job')->with('status', "Uploaded with success!");

    }

    public function displaypdfs() {
        $files = File::files(public_path('files/'));
        return view('pdfFiles', [
            'files' => $files
        ]);
    }

    public function getDownload($file_name)
    {
        $file_path = public_path('files/'.$file_name);
        return response()->download($file_path);
    }
}
