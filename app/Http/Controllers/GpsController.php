<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class GpsController extends Controller
{
    public function getData()
    {
        $trucks = DB::table('trucks')->get();
        return view('trucks', ['trucks' => $trucks]);
    }

    static protected function getETAbyID($id) {
        $eta = DB::table('eta')->where('truck_id', '=', $id)->get();
		if(isset($eta[0]))
            $eta = [
                "eta_time" => $eta[0]->eta_duration,
                "eta_miles" => $eta[0]->eta_destination,
            ];
        else
            $eta = [
                "eta_time" => "Reloading",
                "eta_miles" => "Reloading"
            ];
        return $eta;
    }

    private function getDestinatin($id) {
        $dest = DB::table('job_status')->where('truck_id', '=', $id)->get();
        if(isset($dest[0])) {
            return $dest[0]->post_code;
        }
    }

    public function updateTrucks()
    {
        $trucks = DB::table('trucks')->get();
        foreach ($trucks as $truck) {
            $eta = $this->getETAbyID($truck->id);
            $truck->destination = $this->getDestinatin($truck->id);
            $truck->eta_hours = $eta['eta_time'];
            $truck->eta_miles = $eta['eta_miles'];
        }
        return $trucks;
    }

    public function vbsDate($name) {
        $truck_id = DB::table('trucks')
            ->where('name',"=",$name)
            ->get();
        $truck_id = $truck_id[0]->id;
        $indice_id = DB::table('job_status')
            ->where('truck_id',"=",$truck_id)
            ->get();
        $vbs = DB::table('info_cards')
            ->where('indice_id',"=",$indice_id[0]->id)->get();
        return $vbs;

    }
}
