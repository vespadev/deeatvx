<?php

namespace App\Http\Controllers;

use App\Models\Drivers;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;

class DriversController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function store(Request $request)
    {
        $driver = new Drivers();
        $driver->fill(['name' =>$request->input('name')]);
        $driver->fill(['phone' =>$request->input('phone')]);
        $driver->save();
        return redirect('/drivers')->with('message', "Was created success!");
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Drivers $drivers
     * @return Response
     */
    public function show(Drivers $drivers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param integer
     * @return Application|Factory|View|Response
     */
    public function edit($id)
    {
        $data = Drivers::find($id);
        return view('forms.edit-driver',['driver'=> $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param \App\Models\Drivers $drivers
     * @return Response
     */
    public function update(Request $request)
    {
        $driver = Drivers::find($request->input('id'));
        $driver->fill(['name' =>$request->input('name')]);
        $driver->fill(['phone' =>$request->input('phone')]);
        $driver->save();
        return redirect('/drivers')->with('message', "Was updated success!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy($id)
    {
        Drivers::destroy($id);
        return redirect('/drivers')->with('message', "Was updated success!");
    }
}
