<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


// Display Dashboard
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/dashboard', "\App\Http\Controllers\Controller@index")
    ->name('dashboard');
// Display Overview
Route::middleware(['auth:sanctum', 'verified'])
    ->match(['GET','POST'],'/overview', "\App\Http\Controllers\Controller@overview")
    ->name('overview');

// List all trucks
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/trucks', "\App\Http\Controllers\GpsController@getData")
    ->name('trucks');

// Import trucks
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/import-trucks', "\App\Http\Controllers\GpsController@updateTrucks")
    ->name('updateTrucks');
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/import-vbs/{name}', "\App\Http\Controllers\GpsController@vbsDate")
    ->name('vbsDate');

// List all drivers
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/drivers', function () {
        $drivers = DB::table('drivers')
            ->get();
        return view('drivers', ['drivers' => $drivers]);
    })->name('drivers');

// Redirect to form add driver
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/form-add-driver', function () {
    return view('forms.add-driver');
})->name('forms.add-driver');

Route::middleware(['auth:sanctum', 'verified'])
    ->get('/form-edit-driver/{indice}', 'App\Http\Controllers\DriversController@edit')->name('forms.edit-driver');
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/destroy-driver/{indice}', 'App\Http\Controllers\DriversController@destroy')->name('destroy-driver');

// Save new driver
Route::middleware(['auth:sanctum', 'verified'])
    ->post('/save-driver', "\App\Http\Controllers\DriversController@store")
    ->name('save-driver');

Route::middleware(['auth:sanctum', 'verified'])
    ->post('/update-driver', "\App\Http\Controllers\DriversController@update")
    ->name('update-driver');
///////////////////////////////////////////////////////////////////////////////////////
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/jobs', 'App\Http\Controllers\JobsController@index')->name('jobs');
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/displaypdfs', 'App\Http\Controllers\JobsController@displaypdfs')->name('displaypdfs');
Route::get('/send-job', function () {
    return view('forms.send-job');
})->name('send-job');
// Redirect to form
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/form-add-job', function () {
    $drivers = DB::table('drivers')->get();
    $list = DB::table('status_list')->get();
    $trucks = DB::table('trucks')->get();
    $indici = DB::table('indici')->get();
    return view('forms.add-job', [
        'drivers' => $drivers,
        'status_list' => $list,
        'trucks' => $trucks,
        'indici' => $indici
    ]);
})->name('forms.add-job');

// Save new job
Route::post('/save-job', "\App\Http\Controllers\JobsController@createJob")
    ->name('save-job');

Route::middleware(['auth:sanctum', 'verified'])
    ->post('/save-job-pdf', "\App\Http\Controllers\JobsController@createJobPdf")
    ->name('save-job-pdf');

Route::get('/edit-job/{id}', 'App\Http\Controllers\JobsController@edit')->name('forms.edit-job');
Route::post('/update-job', "\App\Http\Controllers\JobsController@update")
    ->name('update-job');

Route::middleware(['auth:sanctum', 'verified'])
    ->get('/form-edit-status/{id}', 'App\Http\Controllers\Controller@changeStatus')->name('update-status');

Route::middleware(['auth:sanctum', 'verified'])
    ->post('/update-delivery-address', 'App\Http\Controllers\Controller@updateAddress')->name('update-delivery-address');

Route::middleware(['auth:sanctum', 'verified'])
    ->get('/form-update-address/{id}', 'App\Http\Controllers\Controller@updateAddressForm')->name('forms.update-address');

Route::middleware(['auth:sanctum', 'verified'])
    ->get('/history', function () {
   return view('history');
});
Route::post('/save-history', 'App\Http\Controllers\Controller@saveHistory')->name('save-history');
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/form-add-history', 'App\Http\Controllers\Controller@addHistory')->name('forms.add-history');
Route::middleware(['auth:sanctum', 'verified'])
    ->get('/download/{file}', 'App\Http\Controllers\JobsController@getDownload');
